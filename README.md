# Gadget App

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/gadget-app.git`
2. Navigate to the folder: `cd gadget-app`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser then type: http://locahost:8080/gadget-console to open H2 Console
5. Open POSTMAN App then import the POSTMAN Collection file
6. Lets try with http://localhost:8080/gadget-gallery, it will be redirected
   to http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config

### Images screen shot

H2 Console

![H2 Console](img/h2.png "H2 Console")

![Gadget Table](img/rows.png "Gadget Table")

Add New Gadget

![Add New Gadget](img/add.png "Add New Gadget")

Get All Gadgets

![Get All Gadgets](img/list.png "Get All Gadgets")

Find Gadget by ID

![Find Gadget by ID](img/find.png "Find Gadget by ID")

Update Gadget by ID

![Update Gadget by ID](img/update.png "Update Gadget by ID")

Delete Gadget by ID

![Delete Gadget by ID](img/delete1.png "Delete Gadget by ID")

Delete All Gadgets

![Delete All Gadgets](img/delete-all.png "Delete All Gadgets")

Swagger UI

![Swagger UI](img/Swagger-UI.png "Swagger UI")