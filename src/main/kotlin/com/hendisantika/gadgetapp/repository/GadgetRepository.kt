package com.hendisantika.gadgetapp.repository

import com.hendisantika.gadgetapp.entity.Gadget
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Created by IntelliJ IDEA.
 * Project : gadget-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/05/21
 * Time: 09.42
 */
@Repository
interface GadgetRepository : JpaRepository<Gadget, Long>