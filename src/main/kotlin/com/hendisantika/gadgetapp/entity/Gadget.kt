package com.hendisantika.gadgetapp.entity

import javax.persistence.*

/**
 * Created by IntelliJ IDEA.
 * Project : gadget-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/05/21
 * Time: 09.32
 */
@Entity
@Table(name = "GADGET")
data class Gadget(

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val gadgetId: Long,
    val gadgetName: String,
    val gadgetCategory: String?,
    val gadgetAvailability: Boolean = true,
    val gadgetPrice: Double
) {
    constructor() : this(
        0, "",
        "", true,
        0.0
    )
}
