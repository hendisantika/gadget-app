package com.hendisantika.gadgetapp.controller

import com.hendisantika.gadgetapp.entity.Gadget
import com.hendisantika.gadgetapp.repository.GadgetRepository
import org.apache.commons.lang3.ObjectUtils
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder

/**
 * Created by IntelliJ IDEA.
 * Project : gadget-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/05/21
 * Time: 09.44
 */
@RestController
@RequestMapping("/api/gadgets")
class GadgetController(private val gadgetRepository: GadgetRepository) {
    @GetMapping
    fun fetchGadgets(): ResponseEntity<List<Gadget>> {
        val gadgets = gadgetRepository.findAll()
        if (gadgets.isEmpty()) {
            return ResponseEntity<List<Gadget>>(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity<List<Gadget>>(gadgets, HttpStatus.OK)
    }

    @GetMapping("/{id}")
    fun fetchGadgetById(@PathVariable("id") gadgetId: Long): ResponseEntity<Gadget> {
        val gadget = gadgetRepository.findById(gadgetId)
        if (gadget.isPresent) {
            return ResponseEntity<Gadget>(gadget.get(), HttpStatus.OK)
        }
        return ResponseEntity<Gadget>(HttpStatus.NOT_FOUND)
    }

    @PostMapping
    fun addNewGadget(@RequestBody gadget: Gadget, uri: UriComponentsBuilder): ResponseEntity<Gadget> {
        val persistedGadget = gadgetRepository.save(gadget)
        if (ObjectUtils.isEmpty(persistedGadget)) {
            return ResponseEntity<Gadget>(HttpStatus.BAD_REQUEST)
        }
        val headers = HttpHeaders()
        headers.location = uri.path("/gadget/{gadgetId}").buildAndExpand(gadget.gadgetId).toUri()
        return ResponseEntity(headers, HttpStatus.CREATED)
    }

    @PutMapping("/{id}")
    fun updateGadgetById(@PathVariable("id") gadgetId: Long, @RequestBody gadget: Gadget): ResponseEntity<Gadget> {
        return gadgetRepository.findById(gadgetId).map { gadgetDetails ->
            val updatedGadget: Gadget = gadgetDetails.copy(
                gadgetCategory = gadget.gadgetCategory,
                gadgetName = gadget.gadgetName,
                gadgetPrice = gadget.gadgetPrice,
                gadgetAvailability = gadget.gadgetAvailability
            )
            ResponseEntity(gadgetRepository.save(updatedGadget), HttpStatus.OK)
        }.orElse(ResponseEntity<Gadget>(HttpStatus.INTERNAL_SERVER_ERROR))
    }

    @DeleteMapping("/{id}")
    fun removeGadgetById(@PathVariable("id") gadgetId: Long): ResponseEntity<Void> {
        val gadget = gadgetRepository.findById(gadgetId)
        if (gadget.isPresent) {
            gadgetRepository.deleteById(gadgetId)
            return ResponseEntity<Void>(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @DeleteMapping
    fun removeAllGadgets(): ResponseEntity<Void> {
        gadgetRepository.deleteAll()
        return ResponseEntity<Void>(HttpStatus.OK)
    }
}