package com.hendisantika.gadgetapp

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@OpenAPIDefinition
@SpringBootApplication
class GadgetAppApplication

fun main(args: Array<String>) {
    runApplication<GadgetAppApplication>(*args)
}
